<?php

namespace Training\AdditionalTestOM\Model;

use Training\TestOM\Model\ManagerInterface;

/**
 * Class ManagerCustom
 * @package Training\AdditionalTestOM\Model
 */
class ManagerCustom implements ManagerInterface
{
    /**
     * @return string|void
     */
    public function get()
    {

    }

    /**
     * @return mixed|void
     */
    public function create()
    {

    }
}
