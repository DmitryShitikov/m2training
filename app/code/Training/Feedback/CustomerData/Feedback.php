<?php

namespace Training\Feedback\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;

/**
 * Class Feedback
 * @package Training\Feedback\CustomerData
 */
class Feedback implements SectionSourceInterface
{

    /**
     * @var \Training\Feedback\Model\ResourceModel\Feedback
     */
    private $feedbackResource;

    /**
     * Feedback constructor.
     * @param \Training\Feedback\Model\ResourceModel\Feedback $feedbackResource
     */
    public function __construct(
        \Training\Feedback\Model\ResourceModel\Feedback $feedbackResource
    ) {
        $this->feedbackResource = $feedbackResource;
    }

    /**
     * @return array
     */
    public function getSectionData()
    {
        return [
            'allFeedbackNumber' => $this->getAllFeedbackNumber(),
            'activeFeedbackNumber' => $this->getActiveFeedbackNumber()
        ];
    }

    /**
     * @return mixed
     */
    public function getAllFeedbackNumber()
    {
        return $this->feedbackResource->getAllFeedbackNumber();
    }

    /**
     * @return mixed
     */
    public function getActiveFeedbackNumber()
    {
        return $this->feedbackResource->getActiveFeedbackNumber();
    }
}

