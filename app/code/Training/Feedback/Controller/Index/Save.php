<?php

namespace Training\Feedback\Controller\Index;

/**
 * Class Save
 * @package Training\Feedback\Controller\Index
 */
class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Training\Feedback\Model\FeedbackFactory
     */
    private $feedbackFactory;

    /**
     * @var \Training\Feedback\Model\ResourceModel\Feedback
     */
    private $feedbackResource;

    /**
     * @var \Training\Feedback\Model\FeedbackValidate
     */
    private $feedbackValidate;


    /**
     * Save constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Training\Feedback\Model\FeedbackFactory $feedbackFactory
     * @param \Training\Feedback\Model\ResourceModel\Feedback $feedbackResource
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Training\Feedback\Model\FeedbackFactory $feedbackFactory,
        \Training\Feedback\Model\ResourceModel\Feedback $feedbackResource,
        \Training\Feedback\Model\FeedbackValidate $feedbackValidate
    ) {
        parent::__construct($context);
        $this->feedbackFactory = $feedbackFactory;
        $this->feedbackResource = $feedbackResource;
        $this->feedbackValidate = $feedbackValidate;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->resultRedirectFactory->create();

        if ($post = $this->getRequest()->getPostValue()) {
            try {
                $this->feedbackValidate->validatePost($post);
                $feedback = $this->feedbackFactory->create();
                $feedback->setData($post);
                $this->feedbackResource->save($feedback);
                $this->messageManager->addSuccessMessage(__('Thank you for your feedback.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('An error occurred while processing your form. Please try again later.')
                );
                $result->setPath('*/*/form');

                return $result;
            }
        }

        return $result->setPath('*/*/index');
    }
}
