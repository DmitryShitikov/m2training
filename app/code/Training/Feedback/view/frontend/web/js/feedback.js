define([
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'ko'
], function (Component, customerData, ko) {
    'use strict';

    return Component.extend({
        feedbackData: ko.observable(),

        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.feedbackData = customerData.get('feedback');
        }
    });
});
