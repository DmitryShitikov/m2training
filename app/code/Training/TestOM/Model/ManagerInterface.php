<?php

namespace Training\TestOM\Model;

/**
 * Interface ManagerInterface
 * @package Training\TestOM\Model
 */
interface ManagerInterface
{
    /**
     * @return string
     */
    public function get();

    /**
     * @return mixed
     */
    public function create();
}
