<?php

namespace Training\TestOM\Model;

/**
 * Class PlayWithTest
 * @package Training\TestOM\Model
 */
class PlayWithTest implements PlayWithTestInterface
{
    /**
     * @var Test
     */
    private $testObject;

    /**
     * @var TestFactory
     */
    private $testObjectFactory;

    /**
     * @var ManagerCustomImplementation
     */
    private $manager;

    /**
     * PlayWithTest constructor.
     * @param Test $testObject
     * @param TestFactory $testObjectFactory
     * @param ManagerCustomImplementation $manager
     */
    public function __construct(
        \Training\TestOM\Model\Test $testObject,
        \Training\TestOM\Model\TestFactory $testObjectFactory,
        \Training\TestOM\Model\ManagerCustomImplementation $manager
    ) {
        $this->testObject = $testObject;
        $this->testObjectFactory = $testObjectFactory;
        $this->manager = $manager;
    }

    /**
     * @return mixed|void
     */
    public function run()
    {
        $this->testObject->log();
        $customArrayList = ['item1' => 'aaaaa', 'item2' => 'bbbbb'];
        $newTestObject = $this->testObjectFactory->create(['arrayList' => $customArrayList, 'manager' => $this->manager]);
        $newTestObject->log();
    }
}
