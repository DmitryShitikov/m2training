<?php

namespace Training\TestOM\Model;

/**
 * Interface ManagerCustomImplementation
 * @package Training\TestOM\Model
 */
interface ManagerCustomImplementation extends ManagerInterface
{
    /**
     * @return mixed
     */
    public function get();

    /**
     * @return mixed
     */
    public function create();
}