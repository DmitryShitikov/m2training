<?php

namespace Training\TestOM\Model;

/**
 * Interface PlayWithTestInterface
 * @package Training\TestOM\Model
 */
interface PlayWithTestInterface
{
    /**
     * @return mixed
     */
    public function run();
}