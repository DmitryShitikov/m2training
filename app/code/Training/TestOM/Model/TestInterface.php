<?php

namespace Training\TestOM\Model;

/**
 * Interface TestInterface
 * @package Training\TestOM\Model\Test
 */
interface TestInterface
{
    /**
     * @return mixed
     */
    public function log();
}
