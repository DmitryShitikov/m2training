<?php


namespace Training\TestOM\Model;

/**
 * Class Test
 * @package Training\TestOM\Model
 */
class Test implements TestInterface
{
    /**
     * @var ManagerInterface
     */
    private $manager;

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $arrayList;

    /**
     * @var int
     */
    private $number;

    /**
     * @var ManagerInterfaceFactory
     */
    private $managerFactory;

    /**
     * Test constructor.
     * @param ManagerInterface $manager
     * @param ManagerInterfaceFactory $managerFactory
     * @param string $name
     * @param int $number
     * @param array $arrayList
     */
    public function __construct(
        ManagerInterface $manager,
        ManagerInterfaceFactory $managerFactory,
        string $name,
        int $number,
        array $arrayList
    ) {
        $this->manager = $manager;
        $this->managerFactory = $managerFactory;
        $this->name = $name;
        $this->number = $number;
        $this->arrayList = $arrayList;
    }

    /**
     * @return mixed|void
     */
    public function log()
    {
        print_r(get_class($this->manager));
        echo '<br/>';
        print_r(get_class($this->managerFactory));
        echo '<br/>';
        print_r($this->name);
        echo '<br/>';
        print_r($this->arrayList);
        echo '<br/>';
        print_r($this->number);
    }
}
