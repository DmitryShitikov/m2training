<?php

namespace Training\TestOM\Plugin\Block;

/**
 * Class Template
 * @package Training\TestOM\Plugin\Block
 */
class Template
{
    /**
     * @param \Magento\Framework\View\Element\Template $subject
     * @param $result
     * @return string
     */
    public function afterToHtml(
        \Magento\Framework\View\Element\Template $subject,
        $result
    ) {
        if ($subject->getNameInLayout() == 'top.search') {
            $result = '<div><p style="color:red; "><b>' . $subject->getTemplate() . '</b></p>'
                . '<p style="color: green;"><b>' . get_class($subject) . '</b></p>' . $result . '</div>';
        }

        return $result;
    }
}
