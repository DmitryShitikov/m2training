<?php

namespace Training\TestOM\Controller\App\Router;

/**
 * Class NoRouteHandler
 * @package Training\TestOM\Controller
 */
class NoRouteHandler implements \Magento\Framework\App\Router\NoRouteHandlerInterface
{
    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    public function process(\Magento\Framework\App\RequestInterface $request)
    {
        $moduleName = 'cms';
        $actionPath = 'index';
        $actionName = 'index';

        $request->setModuleName($moduleName)
            ->setControllerName($actionPath)
            ->setActionName($actionName);

        return true;
    }
}
