<?php

namespace Training\TestOM\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class LogPageHtml
 * @package Training\TestOM\Observer
 */
class LogPageHtml implements ObserverInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * LogPageHtml constructor.
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $response = $observer->getEvent()->getResponse();
        $this->logger->debug($response->getBody());
    }
}