<?php

namespace Training\Testhomework\Controller\Review;

use Training\Testhomework\Model\Review;

class Index extends \Magento\Framework\App\Action\Action
{
    private $jsonResultFactory;

    private $review;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Training\Testhomework\Model\Review $review,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
    ) {
        $this->jsonResultFactory = $jsonResultFactory;
        $this->review = $review;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->jsonResultFactory->create();
        $result->setData(json_encode($this->review->getRandomReviewData()));

        return $result;
    }
}
