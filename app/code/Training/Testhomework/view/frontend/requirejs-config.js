var config = {
    config: {
        mixins: {
            'Magento_Catalog/js/catalog-add-to-cart': {
                'Training_Testhomework/js/catalog-add-to-cart': true
            },
            'Magento_Checkout/js/action/place-order': {
                'Training_Testhomework/js/checkout/action/place-order': true
            }
        }
    }
};