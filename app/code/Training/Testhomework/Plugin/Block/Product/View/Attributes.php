<?php

namespace Training\Testhomework\Plugin\Block\Product\View;

/**
 * Class Description
 * @package Training\Testhomework\Plugin\Block\Product\View
 */
class Attributes
{
    private $logger;

    public function __construct(
      \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Catalog\Block\Product\View\Attributes $subject
     */
    public function beforeToHtml(
        \Magento\Catalog\Block\Product\View\Attributes $subject
    ) {
        $this->logger->debug("Block: " . $subject->getTemplate() . " File: " . $subject->getTemplateFile() . "\n");
    }
}
