<?php

namespace Training\Testhomework\Plugin\Block\Product\View;

/**
 * Class Description
 * @package Training\Testhomework\Plugin\Block\Product\View
 */
class Description
{
    /**
     * @param \Magento\Catalog\Block\Product\View\Description $subject
     */
    public function beforeToHtml(
        \Magento\Catalog\Block\Product\View\Description $subject
    ) {
        if ($subject->getNameInLayout() !== 'product.info.sku') {
            $subject->setTemplate('Training_Testhomework::description.phtml');
        }
    }
}
