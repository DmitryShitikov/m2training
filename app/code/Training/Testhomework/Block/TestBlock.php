<?php

namespace Training\Testhomework\Block;


class TestBlock extends \Magento\Framework\View\Element\AbstractBlock
{
    /**
     * @return string
     */
    public function toHtml()
    {
        return "<b>Hello world from block!</b>";
    }
}